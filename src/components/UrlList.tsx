import React from 'react';

import List from '../atoms/List';
import ListItem from '../atoms/ListItem';
import clsx from 'clsx';
import Icon from '../atoms/Icon';

import classes from './styles/PasswordEdit.module.css';

interface UrlListProps {
    urls: string[];
    onDelete: (index: number) => () => void;
}

function UrlList({ urls, onDelete }: UrlListProps) {
    return (
        <List className={classes.urlList}>
            {urls?.map((urlEntry, index) => (
                <ListItem key={index} dense className={classes.urlListItem}>
                    <>{urlEntry}</>
                    <Icon onClick={onDelete(index)} size="small" className="fas fa-trash" />
                </ListItem>
            ))}
            {urls?.length === 0 && (
                <ListItem dense className={clsx(classes.urlListItem, classes.urlListItemEmpty)}>
                    No urls added
                </ListItem>
            )}
        </List>
    );
}

export default React.memo(UrlList);
