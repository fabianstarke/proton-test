import React from 'react';
import clsx from 'clsx';

import classes from './styles/ListItem.module.css';

interface Props extends React.ComponentPropsWithoutRef<'li'> {
    clickable?: boolean;
    dense?: boolean;
}

function ListItem({ className, clickable, dense, onClick, ...rest }: Props) {
    const rootClassName = clsx(className, classes.root, {
        [classes.clickable]: clickable,
        [classes.dense]: dense,
    });

    function handleClick(e: React.MouseEvent<HTMLLIElement, MouseEvent>) {
        onClick && onClick(e);

        const { currentTarget: node } = e;

        const selectedClass = classes.selected;
        const selectedNodes = Array.from(node.parentElement?.children ?? []).filter((child) =>
            child.classList.contains(selectedClass)
        );
        selectedNodes.forEach((selectedNode) => selectedNode.classList.remove(selectedClass));
        node.classList.add(selectedClass);
    }

    return <li className={rootClassName} onClick={handleClick} {...rest} />;
}

export default ListItem;
